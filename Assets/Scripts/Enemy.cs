﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Color AliveColor;
    public Color DeadColor;

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private float _movementSpeed = 1f;
    [SerializeField] private Transform player;

    private bool _isAlive;

    public void Initialize(Transform player)
    {
        this.player = player;
    }

    public void Setup()
    {
        _isAlive = true;
        _spriteRenderer.color = AliveColor;

        transform.position = new Vector3(Random.Range(-GameController.I.ScreenBounds.x, GameController.I.ScreenBounds.x), Random.Range(-GameController.I.ScreenBounds.y, GameController.I.ScreenBounds.y), 0f);
    }

    private void Update()
    {
        if (!GameController.I.GameInProgress) return;
        if (!_isAlive) return;

        var amount = ((player.position) - (transform.position)).magnitude;
        var direction = ((player.position) - (transform.position)).normalized;
        if (amount < 1f && amount >= 0.1f)
        {
            transform.position += -direction * _movementSpeed * Time.deltaTime;
        }
        else if (amount < 0.1f)
        {
            Die();
            return;
        }

        // Check if in camera boundaries
        if (transform.position.x < -GameController.I.ScreenBounds.x)
        {
            transform.position += new Vector3(1f, 0f, 0f) * _movementSpeed * Time.deltaTime;
        }

        if (transform.position.x > GameController.I.ScreenBounds.x)
        {
            transform.position += new Vector3(-1f, 0f, 0f) * _movementSpeed * Time.deltaTime;
        }

        if (transform.position.y < -GameController.I.ScreenBounds.y)
        {
            transform.position += new Vector3(0f, 1f, 0f) * _movementSpeed * Time.deltaTime;
        }

        if (transform.position.y > GameController.I.ScreenBounds.y)
        {
            transform.position += new Vector3(0f, -1f, 0f) * _movementSpeed * Time.deltaTime;
        }
    }

    private void Die()
    {
        _isAlive = false;
        _spriteRenderer.color = DeadColor;
    }
}
