﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _movementSpeed = 1f;

    private void Update()
    {
        if (!GameController.I.GameInProgress) return;

        if (Input.GetKey("escape"))
        {
            GameController.I.EndGame();
            return;
        }

        var movementX = Input.GetAxis("Horizontal");
        var movementY = Input.GetAxis("Vertical");

        transform.position += new Vector3(movementX, movementY, 0) * Time.deltaTime * _movementSpeed;

        if (transform.position.x < -GameController.I.ScreenBounds.x)
        {
            transform.position += new Vector3(1f, 0f, 0f) * _movementSpeed * Time.deltaTime;
        }

        if (transform.position.x > GameController.I.ScreenBounds.x)
        {
            transform.position += new Vector3(-1f, 0f, 0f) * _movementSpeed * Time.deltaTime;
        }

        if (transform.position.y < -GameController.I.ScreenBounds.y)
        {
            transform.position += new Vector3(0f, 1f, 0f) * _movementSpeed * Time.deltaTime;
        }

        if (transform.position.y > GameController.I.ScreenBounds.y)
        {
            transform.position += new Vector3(0f, -1f, 0f) * _movementSpeed * Time.deltaTime;
        }
    }
}
