﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private EventSystem m_EventSystem;

    [SerializeField] private Canvas _mainMenuCanvas;
    [SerializeField] private Canvas _settingsCanvas;

    [SerializeField] private GameObject _playButton;
    [SerializeField] private GameObject _checkbox1;

    public void PlayButtonClicked()
    {
        _mainMenuCanvas.enabled = false;

        m_EventSystem.SetSelectedGameObject(null);

        GameController.I.StartGame();
    }

    public void OpenMainMenu()
    {
        _mainMenuCanvas.enabled = true;

        m_EventSystem.SetSelectedGameObject(_playButton);
    }

    public void OpenSettings()
    {
        _mainMenuCanvas.enabled = false;
        _settingsCanvas.enabled = true;

        m_EventSystem.SetSelectedGameObject(_checkbox1);
    }

    public void CloseSettings()
    {
        _mainMenuCanvas.enabled = true;
        _settingsCanvas.enabled = false;

        m_EventSystem.SetSelectedGameObject(_playButton);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    void Update()
    {
        if (Input.anyKeyDown && !(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)))
        {
            if (_mainMenuCanvas.enabled && m_EventSystem.currentSelectedGameObject == null)
            {
                m_EventSystem.SetSelectedGameObject(_playButton);
            }
            else if (_settingsCanvas.enabled && m_EventSystem.currentSelectedGameObject == null)
            {
                m_EventSystem.SetSelectedGameObject(_checkbox1);
            }
        }
        
        if (Input.GetKey("escape"))
        {
            if (_mainMenuCanvas.enabled) return;

            CloseSettings();
        }
    }
}
