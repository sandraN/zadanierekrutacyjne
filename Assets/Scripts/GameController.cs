﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    public static GameController I { get; private set; }

    public UnityEvent OnEndGame = new UnityEvent();

    public int EnemiesCount;
    public Vector2 ScreenBounds;
    public bool GameInProgress;

    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _enemyPrefab;

    [SerializeField] private Transform _enemiesParent;

    private Player _player;
    private List<Enemy> _enemies = new List<Enemy>();


    private void Start()
    {
        I = this;

        ScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    [ContextMenu("Start Game")]
    public void StartGame()
    {
        if (_player == null)
        {
            _player = Instantiate(_playerPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity).GetComponent<Player>();
            _player.transform.SetParent(this.transform);
        }
        else
        {
            _player.transform.position = new Vector3(0f, 0f, 0f);
        }

        while (_enemies.Count < EnemiesCount)
        {
            var enemy = Instantiate(_enemyPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity).GetComponent<Enemy>();
            enemy.transform.SetParent(_enemiesParent);
            enemy.Initialize(_player.transform);
            _enemies.Add(enemy);
        }

        foreach (var enemy in _enemies)
        {
            enemy.Setup();
        }

        GameInProgress = true;
    }

    public void EndGame()
    {
        GameInProgress = false;

        OnEndGame.Invoke();
    }
}
